=begin #comments
                             StoreTimeLister

            StoreTimeLister is an app that reads a .csv
            file and outputs a file by open or close times
            by day of week.

            Usage:
            Place the CSV files in the "01_files_to_process"
            directory & run the process (seen in the line below.)
            $ruby storetimelister.rb

            Version:             1.00
            Last Revision:       13-Sep-2014
            Author:              David Teren
            Copyright:           Radio Retail (Pty) Ltd


=end

=begin #comments
        Known Issues:

        -> CSV files must be UTF-8 compliant or results in a read error
        ->
        ->

=end

=begin #Comments
        To Do List:

        -> Use methods - Done!
        -> Put methods into Classes
        -> File path stuff - Done!
        -> Method to go through the dir & process all the found files
        -> Select a  folder and process contents?
        -> Non UTF-8 CSV issue
        -> Set paths and file management for use across platforms
        -> Implement SmarterCSV Gem ? may solve the UTF issue?
        -> Better File Management - InProgress!
        -> Read directly from xls files

=end #

require "CSV"
require "pathname"

## Set the date & time for stamping the output folder
date_stamp = Time.now.strftime("%Y-%m-%d_%H-%M")

# handy to display a var for test/debug purposes
def echo(put_this)
  line =  ("\n" * 2) + ("_" * 60) + ("\n" * 4)
    puts line
    puts put_this
    puts line
end

# Set the path relative to the app
def pathfinder(the_path)
    a_path = File.dirname(__FILE__)
    the_path = File.join(a_path + the_path)
end

# Check for & create dir
def checkmakedir(dir_path)
     Dir.mkdir (dir_path) if !File.directory?(dir_path)
end

def opentime(row, time, rownum, open_or_close)
  out_dir = "01OpenTimes"         ## sets the default output directory
  day = @week_days[rownum  - 2]
  x = row[rownum].split("-")
  y = x[(open_or_close).to_i].strip.upcase ## open_or_close -> 0 for open time / 1 for close time
  case
      when y == time ; i = row[0] + ", " + row[1] + ", " + y
      file_num = (rownum - 1).to_s.rjust(2,"0")
      if open_or_close == "0" ; out_dir = "01OpenTimes" ; else out_dir = "02Closetimes" ; end
      the_file = "#{@path_out}#{out_dir}/#{file_num}_#{day}_#{time}#{@file_ext}"
      f = File.open(the_file, 'a') ;  f.puts i ; f.close
  end
 end

path_jobs_to_proc = pathfinder("/jobs/01_files_to_process")

list_of_files = Dir.glob("#{path_jobs_to_proc}/*")
if list_of_files.length == 0 ; then echo("No files found to process.") & exit ; end

path_done_files = pathfinder("/jobs/02_processed_files/")
@path_out = pathfinder("/jobs/03_outputs/#{date_stamp}/")

checkmakedir("#{@path_out}/")
checkmakedir("#{@path_out}/01OpenTimes")
checkmakedir("#{@path_out}/02Closetimes")

@week_days = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]

## set the output file extension
@file_ext = ".txt"

## Go through the list of files to be processed
lc  = list_of_files.length
lc.times do |m|
    the_file = list_of_files[m]

## Get the filename to move the file later
    file_name = File.basename(the_file)

    ## Read the CSV file routine
    CSV.foreach(the_file, "r:ISO-8859-1") do |row| ## read the csv file one line at a time

        ## Store open times process -> check for matching store times
        open_times = ["07H00", "07H30", "08H00", "08H30", "09H00", "09H30", "10H00"] ## the list of store open time slots
            n = open_times.length 	    ## get the number of close time slots
            n.times do |p| 			        ## repeat through the time slots
                (2..8).each do |i|    	## repeat through the week days
                    opentime(row, open_times[p], i, "0") ## calls the "opentime" method parsing each line from the CSV
                end
            end

        ## Store close times process
        open_times = ["13H00", "13H30", "14H00", "14H30", "15H00", "15H30", "16H00", "16H30", "17H00", "17H30", "18H00", "18H30", "19H00", "19H30", "20H00", "20H30", "21H00", "21H30", "22H00", "22H30", "23H00"]                      ## the list of store close time slots
            n = open_times.length 	    ## get the number of open time slots
            n.times do |p| 			        ## repeat by that number
                (2..8).each do |i| 	    ## a loop within a loop
                    opentime(row, open_times[p], i, "1") ## calls the "opentime" method
                end
            end
    end

   ## Move the processed file to the "02_processed_files" directory
  File.rename(the_file, "#{path_done_files}#{file_name}")
end
