
                             StoreTimeLister

            StoreTimeLister is an app that reads a .csv
            file and outputs a file by open or close times
            by day of week.

            Usage:
            Place the CSV files in the "01_files_to_process"
            directory & run the process (seen in the line below.)
            $ruby storetimelister.rb

            Version:             1.00
            Last Revision:       13-Sep-2014
            Author:              David Teren
            Copyright:           Radio Retail (Pty) Ltd





Known Issues:

        -> CSV files must be UTF-8 compliant or results in a read error





To Do List:

        -> Use methods - Done!
        -> Put methods into Classes
        -> File path stuff - Done!
        -> Method to go through the dir & process all the found files
        -> Select a  folder and process contents?
        -> Non UTF-8 CSV issue
        -> Set paths and file management for use across platforms
        -> Implement SmarterCSV Gem ? may solve the UTF issue?
        -> Better File Management - InProgress!
        -> Read directly from xls files
